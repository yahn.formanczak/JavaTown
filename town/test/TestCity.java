package town.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import town.City;
import town.Person;
import town.exception.EmptyCityException;

class TestCity {

	@Test
	void testCity() {
		City c;
		
		// Attendus
		int population=0;
		int food=0;
		
		//Scenario
		c=new City();		
		
		//Vérifications
		TestCase.assertEquals(population, c.getPopulation());
		TestCase.assertEquals(food, c.getFood());
	}

	@Test
	void testGetPopulation() {
		City c;
		
		// Attendus
		int population=3;
		
		//Scenario
		c=new City();
		c.addPerson(new Person());
		c.addPerson(new Person());
		c.addPerson(new Person());
		
		//Vérifications
		TestCase.assertEquals(population, c.getPopulation());
	}
	

	@Test
	void testNewYear() {
		City c;
		
		// Attendus
		double age=1.0;
		
		//Scenario
		c=new City();
		
		try {
			TestCase.assertEquals(age, c.getAgeAverage());
			fail();
		} catch (EmptyCityException e) {
			
		}
		
		c.addPerson(new Person());
		c.addPerson(new Person());
		c.addPerson(new Person());
		c.newYear();
		
		//Vérifications
		try {
			TestCase.assertEquals(age, c.getAgeAverage());
		} catch (EmptyCityException e) {
			fail();
		}
	}
	
}
