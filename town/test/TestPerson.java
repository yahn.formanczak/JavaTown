package town.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import town.Person;
import town.exception.InsufficientHappinessException;

class TestPerson {

	@Test
	public void testPersonDefault(){
		// Variables de travail
		Person h;
		
		// Valeurs � tester
		boolean isHappy=true;
		int age=0;
		
		// Situation test�e
		h=new Person();
		
		// Assertions
		TestCase.assertEquals(isHappy, h.isHappy());
		TestCase.assertEquals(age, h.getAge());
	}

	
	@Test
	public void testPerson(){
		//Variables de travail
		Person h;
		
		//Valeurs � tester
		boolean isHappy=true;
		int age=20;
		
		//Situation test�e
		h=new Person(age);
		
		// Assertions
		TestCase.assertEquals(isHappy, h.isHappy());
		TestCase.assertEquals(age, h.getAge());
	}
	
	@Test
	public void testGrow(){
		//Variables de travail
		Person h;
		
		//Valeurs � tester
		boolean isHappy=true;
		int age=20;
		
		//Situation test�e
		h=new Person(age);
		h.grow();
		
		// Assertions
		TestCase.assertEquals(isHappy, h.isHappy());
		TestCase.assertEquals(age+1, h.getAge());
	}
	

	@Test
	public void testWork(){
		//Variables de travail
		Person h;
		
		//Valeurs � tester
		boolean isHappy=false;
		
		//Situation test�e
		h=new Person();
		try {
			h.work();
		}
		catch(InsufficientHappinessException e) {
			fail();
		}
		
		// Assertions
		TestCase.assertEquals(isHappy, h.isHappy());
		
		try {
			h.work();
			fail();
		}
		catch(InsufficientHappinessException e) {
			
		}
	}
}
