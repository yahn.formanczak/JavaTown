package town;

public class MainProgram {

	public static void main(String[] args) {
		City c = new City();
		int initialPopulation=5;
		int yearsToRun=5;
		
		System.out.println(c.toString());
		
		for(int i=0; i<initialPopulation; i++) {
			c.addPerson(new Person());
		}		
		System.out.println(c.toString());
		
		for(int i=0; i<yearsToRun; i++) {

			c.newYear();
			c.addPerson(new Person());
			c.work();
			c.banquet();
			
			System.out.println(c.toString());
		}

	}

}
