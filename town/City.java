package town;

import java.util.ArrayList;

import town.exception.EmptyCityException;

public class City {
	ArrayList<Person> population;
	int food;
	
	public City() {
		population=new ArrayList<Person>();
		food=0;
	}
	
	public int getPopulation() {
		return population.size();
	}
	
	public int getFood() {
		return food;
	}
	
	public int getHappinessIndex() throws EmptyCityException{
		int sumHappiness=0;
		
		if(getPopulation()==0) {
			throw new EmptyCityException();
		}
		
		for(int i=0; i<population.size(); i++) {
			if(population.get(i).isHappy()) {
				sumHappiness++;
			}
		}
		return sumHappiness*100/population.size();
	}
	

	public double getAgeAverage() throws EmptyCityException{
		
		if(getPopulation()==0) {
			throw new EmptyCityException();
		}
		
		int sumAge=0;
		for(int i=0; i<population.size(); i++) {
				sumAge+=population.get(i).getAge();
		}
		return (double)sumAge/(double)population.size();
	}
	
	public void addPerson(Person p) {
		population.add(p);
	}
	
	public void newYear() {
		for(int i=0; i<population.size(); i++) {
			population.get(i).grow();
		}
	}
	
	public void work() {
		for(int i=0; i<population.size(); i++) {
			if(population.get(i).isHappy()) {
				population.get(i).grow();
				food+=3;
			}
		}
	}
	
	public void banquet() {
		for(int i=0; i<population.size(); i++) {
			if(food>0) {
				population.get(i).eat();
				food--;
			}
		}
	}
	
	public String toString() {
		String txt="=== City ==="+"\n";
		try {
		txt+="Population:"+getPopulation()+"\n";
		txt+="Food: "+getFood()+"\n";
		txt+="Happiness index: "+getHappinessIndex()+"%\n";
		txt+="Age average: "+getAgeAverage()+"\n";
		}
		catch(EmptyCityException e) {
			txt+="Ghost town!\n";
		}
		txt+="============\n";
		return txt;
	}


}
