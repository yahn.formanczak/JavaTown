package town;

import town.exception.InsufficientHappinessException;

public class Person {
	
	int age;
	boolean happiness;
	
	
	public Person() {
		this(0);
	}
	
	public Person(int pAge) {
		age=pAge;
		happiness=true;
	}
	
	public void eat() {
		happiness=true;
	}
	
	public void grow() {
		age++;
	}
	
	public boolean isHappy() {
		return happiness;
	}
	
	public int getAge() {
		return age;
	}
	
	public void work() throws InsufficientHappinessException {
		if(!happiness) {
			throw new InsufficientHappinessException();
		}
		happiness=false;
	}
	
}
